# COVID-19 Ideas

A collection of many stuff on COVID-19 I have being collecting. 

## 1. Contact Tracing: Test, Trace, Isolate 

* O PROBLEMA: Quarentena e lockdown não acabam com a Covid-19... Somente nos permitem ganhar tempo e diminuir o número de pacientes críticos para que posssamos retomar as rédeas da situação e planejar nossas ações futuras! O que é necessário para realmente resolver a COVID na raiz do problema é o Rastreamento de contatos: Testar, Rastrear e Isolar!!

* Proposta de Solução: COMUNICAÇÃO ADEQUADA: 
  - Mostrar matematicamente que a Covid não vai passar sozinha!
  - Apresentar solução tecnicamente viável
  - Convencimento da população

* Começar pelo lockdown --> Reduzir o número de pacientes críticos (resetar o problema) --> achatar a curva

* Testagem em massa --> detectar os poucos infectados que sobraram

* Rastreamento de contatos --> APLICATIVO: Encontrar possíveis contágios

* Isolamento Efetivo --> Supeorte financeiro

* COMO CONVENCER A INSTALAR O APLICATIVO: 
  - Quem testar positivo (e tiver renda até x salários mínimo) ganha uma cesta básica, na condição de ficar em casa e manter acompanhamento via aplicativo (consultas diárias).
  - Se RASTREADOS testarem positivo, ganham cesta básica para permanecer em casa.

* Como provar que a pessoa está em casa -> aplicativo fará contatos aleatórios e compartilha sua localização.

### Referências:

* What Happens Next - credits to Marcel Salathé & Nicky Case (May 2020) - https://ncase.me/covid-19/?fbclid=IwAR1HS8KSdVxQZZkFI_UVUx_xevMO-0PKhB31Kl-1THwUDWA-J4FaKWLANrY

* The full comic - https://ncase.me/contact-tracing/

* Full Paper on Contact Tracing - Quantifying SARS-CoV-2 transmission suggests epidemic control with digital contact tracing - Science  08 May 2020:
Vol. 368, Issue 6491, eabb6936 DOI: 10.1126/science.abb6936 - https://science.sciencemag.org/content/368/6491/eabb6936

* This Overlooked Variable Is the Key to the Pandemic - https://www.theatlantic.com/health/archive/2020/09/k-overlooked-variable-driving-pandemic/616548/?fbclid=IwAR2eExpdtr955DKu7J_H9juDNbochWnubrB4sq9PO9mbTVch4R0H9U2-0m0

* Coronavírus - SUS - Aplicativo do SUS - https://play.google.com/store/apps/details?id=br.gov.datasus.guardioes&hl=pt_BR&gl=US

* Resenhas de usuários do aplicativo - https://play.google.com/store/apps/details?id=br.gov.datasus.guardioes&hl=pt_BR&gl=US&showAllReviews=true

* Avaliação do Canaltech - Coronavírus - SUS: como descobrir se teve contato com pessoas infectadas - https://canaltech.com.br/apps/como-usar-app-coronavirus-sus/

* Avaliação do Estadão - Coronavírus SUS: veja como funciona o app do governo - https://link.estadao.com.br/noticias/cultura-digital,coronavirus-sus-veja-como-funciona-o-app-do-governo,70003430725

* Avaliação do Estadão - App do SUS vai monitorar coronavírus com tecnologia de Google e Apple - https://link.estadao.com.br/noticias/cultura-digital,app-do-sus-vai-monitorar-coronavirus-com-tecnologia-de-google-e-apple,70003383283

* Avaliação do UOL-Tilt - App Coronavírus SUS agora vai avisar quando usuário foi exposto; entenda - https://www.uol.com.br/tilt/noticias/redacao/2020/07/31/app-coronavirus---sus-adiciona-rastreamento-de-contatos-entenda.htm

* PROJETO - Código Open Source de Aplicativo DP^3T-Decentralized Privacy-Preserving Proximity Tracing - https://github.com/DP-3T


## 2. Políticas durante a pandemia 
Informações sobre políticas adotadas pelos governos federal, estaduais e municipais.

* Jornal da USP: Pesquisa identifica estratégia do Executivo federal em atrapalhar combate à pandemia - 
https://jornal.usp.br/atualidades/pesquisa-identifica-estrategia-do-executivo-federal-em-atrapalhar-combate-a-pandemia/

* Boletim Direitos na Pandemia nº 10 - MAPEAMENTO E ANÁLISE DAS NORMAS JURÍDICAS DE RESPOSTA À COVID-19 NO BRASIL : https://www.conectas.org/publicacoes/download/boletim-direitos-na-pandemia-no-10

## 3. Tratamento Precoce
Todas as mentiras e notícias falsas desmentidas pela ciência!!

* Ivermectina em excesso pode lesionar o fígado, alerta especialista - https://www.em.com.br/app/noticia/bem-viver/2021/02/10/interna_bem_viver,1236698/ivermectina-em-excesso-pode-lesionar-o-figado-alerta-especialista.shtml

## 4. Genetic Algorithm for Epidemic Model parameter adjustment
Esse projeto dos alunos Rafael Pastre e Leonardo Alves aplica um Algoritmo Evolutivo para ajustar e otimisar os parâmetros do modelo SIRD.

* Genetic Algorithm for Epidemic Model parameter adjustment - https://github.com/rafael-pastre/SSC0713-Sistemas-Evolutivos-Epidemico


## 5. Liberdade de Imprensa

* Relatório sobre direitos humanos EUA - https://oglobo.globo.com/mundo/em-relatorio-sobre-direitos-humanos-eua-citam-postura-belica-de-bolsonaro-contra-imprensa-mortes-arbitrarias-por-policiais-24948940

* 2020 Country Reports on Human Rights Practices: Brazil - https://www.state.gov/reports/2020-country-reports-on-human-rights-practices/brazil/

## 6. Compra de Vacinas

![Optional Text](../EduardoSimoes/vacinas.jpg)
